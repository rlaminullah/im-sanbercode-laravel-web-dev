<?php 

	require_once('animal.php');
	require_once('frog.php');
	require_once('ape.php');

	$object = new Animal("Shaun");

	echo "Name : $object->name <br>";
	echo "Legs : $object->legs <br>";
	echo "Cold Blooded : $object->cold_blooded <br><br>";

	$object2 = new Frog("Buduk");
	echo "Name : $object2->name <br>";
	echo "Legs : $object2->legs <br>";
	echo "Cold Blooded : $object2->cold_blooded <br>";
	$object2->lompat();
	echo "<br><br>";

	$object3 = new Ape("Kera Sakti");
	echo "Name : $object3->name <br>";
	echo "Legs : $object3->legs <br>";
	echo "Cold Blooded : $object3->cold_blooded <br>";
	$object3->yell();
	echo "<br><br>";
	
 ?>