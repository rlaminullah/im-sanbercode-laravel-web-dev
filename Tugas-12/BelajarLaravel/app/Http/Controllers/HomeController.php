<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function utama()
    {
        return view('home');
    }

    public function bio()
    {
        return view('form');
    }

    public function kirim(Request $request)
    {
        $frtname = $request['frtname'];
        $lstname = $request['lstname'];

        return view('welcomee',['frtname' => $frtname, 'lstname' => $lstname]);
    }   
}

