<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Akun Baru</h1>

    <h2>Sign Up Form</h2>
    <form action="/kirim" method="post">
    	@csrf
        <label for="">First Name :</label><br>
        <input type="text" name="frtname" id=""><br><br>
        <label for="">Last Name :</label><br>
        <input type="text" name="lstname" id=""><br><br>
        <label for="">Gender</label><br>
        <input type="radio" name="jk" value="1">Male <br>
        <input type="radio" name="jk" value="2">Female <br>
        <input type="radio" name="jk" value="3">Others <br><br>
        <label for="">Nationality : </label><br>
        <select name="nation" id="">
            <option value="Indonesian">Indonesian</option>
            <option value="Korean">Korean</option>
            <option value="Japanese">Japanese</option>
        </select><br><br>
        <label for="">Language Spoken :</label><br>
        <input type="checkbox" name="bahasa" value="Bahasa Indonesia" >Bahasa Indonesia <br>
        <input type="checkbox" name="bahasa" value="English">English <br>
        <input type="checkbox" name="bahasa" value="Others">Others <br><br>
        <label for="">Bio :</label><br>
        <textarea name="" id="" cols="30" rows="10"></textarea> <br><br>
        <a href="/home"><button>Kambali</button></a>
        <input type="submit" name="" id="">

        
    </form>

</body>
</html>